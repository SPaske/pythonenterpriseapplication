# !flask/bin/python
from flask import Flask
from Service.BankAccountService import BankAccountService

app = Flask(__name__)


@app.route('/')
def index():
    return "Hello, World!"


if __name__ == '__main__':
    app.run(debug=True)
