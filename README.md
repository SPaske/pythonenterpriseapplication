# Python Enterprise Example

# General
https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html

# Instal Package
https://www.jetbrains.com/help/pycharm-edu/installing-uninstalling-and-upgrading-packages.html

# Coding Style
  - https://www.python.org/dev/peps/pep-0008/
  - https://stackoverflow.com/questions/159720/what-is-the-naming-convention-in-python-for-variable-and-function-names
  - It uses either camel case or lowercase -> depends on the team

Examples:
 - joined_lower for functions, methods, attributes, variables
 - ALL_CAPS for constants
 - StudlyCaps for classes
 - camelCase only to conform to pre-existing conventions

# Python Env
Download latest from website

# Package Managment
https://tinwhiskers.net/setting-up-your-python-environment-with-pip-virtualenv-and-pycharm-mac/
Use PyCharm -> with a virtual enviornment for packages

# Dependeny Injection
https://github.com/alecthomas/injector

# Unit Testing

# VCS
 - Git - select VCS tab - Select Git as VCS Choice
 - Use Source tree to open up git application
 - open up bitbucket and create new repo with settings
 - use git cmd to add repo
 - commit and pish from source tree :)

# Build Pipeline
 - https://github.com/janakiramm/kubernetes-101
 - https://opensource.com/article/18/1/running-python-application-kubernetes
 - https://www.xenonstack.com/blog/devops/delivery-pipeline-python-flask-ci-cd-kubernetes/

# MicroService Architecture
 - https://medium.com/@umerfarooq_26378/web-services-in-python-ef81a9067aaf
 - https://github.com/uber/Python-Sample-Application/blob/master/app.py
 - https://github.com/warpspeed/python-flask-sample/blob/master/manage.py

# React FrontEnd