class BankAccount:
    def __init__(self):
        self.account_value = 0

    def print_statement(self):
        print("Current value in account is: {} pounds".format(self.account_value))

    def deposit(self, amount):
        self.account_value += amount

    def withdraw(self, amount):
        self.account_value -= amount
